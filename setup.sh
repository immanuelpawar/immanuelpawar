profile="SAP"
terraform_path="deployment/setup"
function configure_aws(){
	echo "Enter AWS Credentials:"
	aws configure --profile $profile
	echo "Enter Again:"
	aws configure --profile $profile
}
if command -v aws --version ; then
	echo "AWS CLI already present"
	configure_aws
else
	echo "Installing AWS CLI"
	apt-get install awscli
	echo "Installation Done"
	configure_aws
fi
cd $terraform_path || exit
export AWS_PROFILE=$profile
#terraform init
#terraform plan -out terraform.tfplan
#terraform apply terraform.tfplan
cd ../..