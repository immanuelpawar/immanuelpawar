FROM python:3.9

WORKDIR /root
COPY . .

RUN pip install -r requirements.txt

EXPOSE 8080

ENTRYPOINT [ "python" ]
CMD ["src/app.py"]
