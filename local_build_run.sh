#!/bin/bash

#docker container stop "migrator"
#docker container stop "/migrator"

sudo service docker stop
#sudo rm -rf /var/lib/docker/
sudo service docker start


export AWS_PROFILE=SAP
export AWS_REGION=us-east-1
export AWS_ACCESS_KEY_ID=$(aws --profile SAP configure get aws_access_key_id)
export AWS_SECRET_ACCESS_KEY=$(aws --profile SAP configure get aws_secret_access_key)

#echo $AWS_ACCESS_KEY_ID 

docker build \
  --build-arg AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
  --build-arg AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
  --build-arg AWS_DEFAULT_REGION=$AWS_REGION . -t dockerpush:latest

sudo docker run -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -dit --restart=always -p 127.0.0.1:8080:8080 -p 8888:8888 --name "skip-a-pay"  dockerpush:latest



