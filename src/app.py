from flask import Flask, request
import datetime
import requests
import xmltodict

# import json
# import boto3
# import pandas as pd

app = Flask(__name__)


@app.route('/')
def index():
    return "hello Team! , These are Fiserv APIs"


@app.route('/getSession', methods=['GET'])
def getsession():
    time = datetime.datetime.now(datetime.timezone.utc).strftime("%Y%j%H%M%S")
    print(time)

    url = "https://cunifydevapi-btat2.fiservapps.com/CUnify/CUnifyAPI"
    payload = getPayload()
    # headers
    headers = {
        'Content-Type': 'text/xml; charset=utf-8'
    }
    response, Session = sessionkey(url, headers, payload)
    return response


def sessionkey(url, headers, payload):
    response = requests.request("POST", url, headers=headers, data=payload, verify=False)
    # prints the response
    print(response.text)
    mystring = response.text
    parts = mystring.split('<sessionKey>')
    part = parts[1].split('</sessionKey>')
    Session = part[0]
    # print(Session)
    print(response)
    obj = xmltodict.parse(response.text)
    return obj, Session


def getPayload():
    time = datetime.datetime.now(datetime.timezone.utc).strftime("%Y%j%H%M%S")
    print(time)

    # structured XML
    payload = """<?xml version="1.0" encoding="UTF-8"?>
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsap="http://wsapi.sos.com/">
            <soapenv:Header/>
            <soapenv:Body>
                <wsap:getSession>
                    <vendorCode>Quadrant</vendorCode>
                    <authentication>
                        <commonClientRT>253177117</commonClientRT>
                        <userLogin>qrUser</userLogin>
                        <userPwd>Test7777!!</userPwd>
                        <utcYYYYdddHHMMSS>""" + time + """</utcYYYYdddHHMMSS>
                        <vendorContext>?</vendorContext>
                    </authentication>
                    <vendorSeq>12345</vendorSeq>
                </wsap:getSession>
            </soapenv:Body>
        </soapenv:Envelope>"""
    return payload


@app.route('/getLoanInfo', methods=['GET'])
def getLoanInformation():
    url = "https://cunifydevapi-btat2.fiservapps.com/CUnify/CUnifyAPI"
    # headers
    headers = {
        'Content-Type': 'text/xml; charset=utf-8'
    }
    payload = getPayload()
    response, Session = sessionkey(url, headers, payload)
    # structured XML
    payload = """<?xml version="1.0" encoding="UTF-8"?>
                    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsap="http://wsapi.sos.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                            <wsap:getLoanInformation>
                                <vendorCode>Quadrant</vendorCode>
                                <sessionId>""" + Session + """</sessionId>
                                <vendorSeq>4654</vendorSeq>
                                <acctId>0</acctId>
                                <suffix>1</suffix>
                                <loanSuffixId>2417542036817196</loanSuffixId>
                            </wsap:getLoanInformation>
                        </soapenv:Body>
                    </soapenv:Envelope>"""
    # POST request
    response = requests.request("POST", url, headers=headers, data=payload, verify=False)
    # prints the response
    print(response.text)
    print(response)
    obj = xmltodict.parse(response.text)
    return obj


@app.route('/modifyLoanRecord', methods=['GET'])
def modifyLoanRecord():
    url = "https://cunifydevapi-btat2.fiservapps.com/CUnify/CUnifyAPI"
    nextPaymentDueDate = request.args.get('nextPaymentDueDate')
    loanSuffixId = request.args.get('loanSuffixId')
    vendorSeq = request.args.get('vendorSeq')
    accountId = request.args.get('accountId')
    # headers
    headers = {
        'Content-Type': 'text/xml; charset=utf-8'
    }
    payload = getPayload()
    response, Session = sessionkey(url, headers, payload)
    # structured XML
    payload = """<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsap="http://wsapi.sos.com/">
    <soapenv:Header/>
    <soapenv:Body>
        <wsap:modifyLoanRecord>
            <vendorCode>Quadrant</vendorCode>
            <sessionId>""" + Session + """</sessionId>
            <vendorSeq>""" + vendorSeq + """</vendorSeq>
            <accountId>""" + accountId + """</accountId>
            <suffix>1</suffix>
            <loanSuffixId>""" + loanSuffixId + """</loanSuffixId>
            <loanRecord>
                <acquisitionAmount>0</acquisitionAmount>
                <advanceDueMaxMonths>6</advanceDueMaxMonths>
                <agreedPayment>1150.00</agreedPayment>
                <appApprovedBy>285485675691387</appApprovedBy>
                <balloonDueDate>1900-01-01T00:00:00-05:00</balloonDueDate>
                <calculatedLastPayment>336.28</calculatedLastPayment>
                <carryOverLateFee>0</carryOverLateFee>
                <carryOverUnpaidInterest>0</carryOverUnpaidInterest>
                <computedPayment>1150.00</computedPayment>
                <costFinancedInsurance>0.00</costFinancedInsurance>
                <creditLeft>0.00</creditLeft>
                <creditLimit>25000.00</creditLimit>
                <creditReviewCode>1</creditReviewCode>
                <creditReviewFrequency>SINGLE_NONE</creditReviewFrequency>
                <currentDelinqDate>2014-09-25T00:00:00-04:00</currentDelinqDate>
                <debtCancelInsureAmount>0.00</debtCancelInsureAmount>
                <decisionCBDate>2009-08-19T00:00:00-04:00</decisionCBDate>
                <decisionCBScore>0</decisionCBScore>
                <decisionCBUserId>285485675691387</decisionCBUserId>
                <drawEndDate>1900-01-01T00:00:00-05:00</drawEndDate>
                <drawMethod>REUSE</drawMethod>
                <gapInsuranceAmount>0.00</gapInsuranceAmount>
                <gapMiscInsuranceUserId>0</gapMiscInsuranceUserId>
                <graceBalance>24530.90</graceBalance>
                <graceDate>2010-03-31T00:00:00-04:00</graceDate>
                <highestBalance>25000.00</highestBalance>
                <indirectLenderName>This is a lender namesss</indirectLenderName>
                <interestCode>DAY365</interestCode>
                <interestOnlyPayment>NO</interestOnlyPayment>
                <lastInterestDate>2018-04-19T00:00:00-04:00</lastInterestDate>
                <lastPaymentAmount>1.00</lastPaymentAmount>
                <lastPaymentDate>2018-04-19T00:00:00-04:00</lastPaymentDate>
                <lateEarned>0</lateEarned>
                <lateFeeAmount>24.00</lateFeeAmount>
                <lateFeeCode>16</lateFeeCode>
                <lateFeeDate>2018-04-21T00:00:00-04:00</lateFeeDate>
                <latePaidPartialAmount>0</latePaidPartialAmount>
                <lifeLate01to29>9</lifeLate01to29>
                <lifeLate30to59>8</lifeLate30to59>
                <lifeLate60to89>2</lifeLate60to89>
                <lifeLate90Plus>18</lifeLate90Plus>
                <loanNumber> </loanNumber>
                <loanOfficer>285485675691387</loanOfficer>
                <MLAEndDate>1900-01-01T00:00:00-05:00</MLAEndDate>
                <MLAStatus>NOTVERIFIED</MLAStatus>
                <MLAStatusDate>1900-01-01T00:00:00-05:00</MLAStatusDate>
                <maturityDate>2016-07-30T00:00:00-04:00</maturityDate>
                <mechanicalBrkDownInsAmt>0.00</mechanicalBrkDownInsAmt>
                <methodMWaitFlag>NO</methodMWaitFlag>
                <minimumPayment>60.00</minimumPayment>
                <nextPaymentDueDate>""" + nextPaymentDueDate + """</nextPaymentDueDate>
                <openEndFlag>YES</openEndFlag>
                <optInOverlimitFee>NO</optInOverlimitFee>
                <originalBalance>25000.00</originalBalance>
                <originalMilitaryAPR>0.0</originalMilitaryAPR>
                <overLimitAmtInAgreedPmt>0.00</overLimitAmtInAgreedPmt>
                <paidOffFlag>NO</paidOffFlag>
                <partialPaymentAmount>439.65</partialPaymentAmount>
                <paybackMinimumPercent>1.2</paybackMinimumPercent>
                <paymentDay>30</paymentDay>
                <paymentFrequency>MONTHLY</paymentFrequency>
                <paymentMethod>U</paymentMethod>
                <payoffDate>1900-01-01T00:00:00-05:00</payoffDate>
                <pledgeCode>NONE</pledgeCode>
                <purposeCode>03</purposeCode>
                <roundPayments>NO</roundPayments>
                <securityCode>SC</securityCode>
                <securityDescription>sec descsssss</securityDescription>
                <skipCode>NO</skipCode>
                <skipEligible>YES</skipEligible>
                <skipEligibleDate>2012-11-30T00:00:00-05:00</skipEligibleDate>
                <skipFeeDate>2012-10-01T00:00:00-04:00</skipFeeDate>
                <skipPaymentDateAdvanced>NO</skipPaymentDateAdvanced>
                <sourceCode>  </sourceCode>
                <studentLoanFeeIndicator>NO</studentLoanFeeIndicator>
                <termByPaymentFrequency>32</termByPaymentFrequency>
                <termInMonths>32</termInMonths>
                <troubledDebtRestructureDate>1900-01-01T00:00:00-05:00</troubledDebtRestructureDate>
                <troubledDebtRestructureStatus>0</troubledDebtRestructureStatus>
                <unpaidInterest>4697.480800</unpaidInterest>
                <variableLoanType>V</variableLoanType>
            </loanRecord>
        </wsap:modifyLoanRecord>
    </soapenv:Body>
</soapenv:Envelope>"""
    # POST request
    response = requests.request("POST", url, headers=headers, data=payload, verify=False)
    # prints the response
    print(response.text)
    print(response)
    obj = xmltodict.parse(response.text)
    return obj


# @app.route('/getDemographics', methods=['GET'])
# def getdemographics():
#     url = "https://cunifydevapi-btat2.fiservapps.com/CUnify/CUnifyAPI"
#
#     # structured XML
#     payload = """<?xml version="1.0" encoding="UTF-8"?>
#     <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsap="http://wsapi.sos.com/">
#         <soapenv:Header />
#         <soapenv:Body>
#             <wsap:getDemographics>
#                 <vendorCode>Quadrant</vendorCode>
#                 <sessionId>49a1cf8cb2d239e23db9cb24e6d7c047405d72f99c8d98c4dae25a5fad6011</sessionId>
#                 <vendorSeq>4654</vendorSeq>
#                 <searchStr>SMITH A</searchStr>
#                 <searchValue>LAST_NAME</searchValue>
#                 <memberFlag>BOTH</memberFlag>
#                 <nonMemFlag>false</nonMemFlag>
#                 <assocConsumerFlag>false</assocConsumerFlag>
#                 <jointFlag>false</jointFlag>
#                 <beneficiaryFlag>false</beneficiaryFlag>
#             </wsap:getDemographics>
#         </soapenv:Body>
#     </soapenv:Envelope>"""
#     # headers
#     headers = {
#         'Content-Type': 'text/xml; charset=utf-8'
#     }
#     # POST request
#     response = requests.request("POST", url, headers=headers, data=payload, verify=False)
#     # prints the response
#     print(json.dumps(response.text))
#     print(response)
#     return jsonify(response.text)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
